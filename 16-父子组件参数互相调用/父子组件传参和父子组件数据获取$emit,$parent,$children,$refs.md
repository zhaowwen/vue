<!--
 * @Author: your name
 * @Date: 2021-08-06 12:03:32
 * @LastEditTime: 2021-08-06 14:07:03
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vuestudy\vue\16-父子组件参数互相调用\父子组件传参和父子组件数据获取$emit,$parent,$children,$refs.md
-->
# $children,$parent,$refs
### 三者实现父子组件数据的获取（只是单纯的获取数据和方法

#### $children:父级组件获取子组件数组的数据和方法
#### $parent: 子级组件获取父级组件的数据和方法
#### $refs子级组件添加参数，父级组件利用$refs获取子级组件对象集

#### 三者区别 $refs获取后的是对象，$children或者$parent获取后的是数组 读取方式不同

# $emit props
### 两者实现父子组件参数的传递（可以改变参数的值
### 子组件给父组件传参用 $emit("方法名"，参数)
### 父组件给子组件传参用 props：{}