/**
 * @description:tab选项卡组件 
 * @author：zww
 */

Vue.component("b-tabs", {
    props: ["list"],
    template: `
      <div class="tab">
      <ul class="title flex">
      <li
          @click='activeindex=i'
          :class="{active:i===activeindex}"
          v-for="(item,i) in cityList"
      >
        {{ item }}
      </li>
      </ul>
      <slot></slot>
      </div>

    `,
    data() {
        return {
            activeindex: 0,
        };
    },
    // 监听 activeindex变化
    watch: {
        activeindex(nval, oval) {
            this.$children[nval].show = true;
            this.$children[oval].show = false
        },
    },
    // 在父组件中获取子组件的内容
    mounted() {
        this.$children[0].show = true;
    }
});