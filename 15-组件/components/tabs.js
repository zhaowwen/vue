/**
 * @description: tabs选项卡组件的选项组件
 * @author: zww
 */
 Vue.component("b-tab-item", {
    template: `
      <div v-show="show">
      <slot></slot>
      </div>
    `,
    mounted() {
        // 在子组件中可以获取到父组件,父组件的data自然也能获取到
        console.log(this.$parent.activeindex);
    },
    data() {
        return {
            show: false,
        }
    }
});