/*
 * @Author: your name
 * @Date: 2021-08-04 17:03:36
 * @LastEditTime: 2021-08-04 17:08:16
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vuestudy\vue\15-组件\components\mycount.js
 */
// 全局组件的定义：components对象定义
Vue.component("b-count", {
    // 注意：模板组件里面必须要有一个根标签 div（不一定是div 一般是div
    // 注意：并且根标签里面不能写v-for指令，写了就不是根标签了
    template: `
    <div>
        <button @click="add">加</button>
        {{mycount}}
        <button @click="reduce">减</button>
    </div>
      `,
    // 传参数的简写，不需要明确参数的类型，默认值等等
    props: ["count"],
    // 如果修改props里面的值代码依旧执行但是会在后台输出Vue警告
    //  Avoid mutating a prop directly since the value will be overwritten whenever the parent component re-renders. Instead, use a data or computed property based on the prop's value. Prop being mutated: "count"
    // 解决办法: 定义data方法接收props参数，并修改data中属性的值
    data() {
      return {
        mycount: this.count,
      };
    },
    methods: {
      add() {
        this.mycount++;
      },
      reduce() {
        this.mycount--;
      },
    },
  });