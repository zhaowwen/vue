/**
 * @description: 搜索框组件
 * @author: zhaowwen
 */
Vue.component("mysearch", {
  props: {
    url: {
      type: String,
    },
    placeholder: {
      type: String,
      default: "请输入搜索内容：",
    },
  },

  template: `
    <div>
        <div class="search">
            <input v-model="searchText" type="search" @keydown.enter="search" :placeholder="placeholder">
            <img :src=url alt="" @click="search">
        </div>
        <button @click="search">搜索</button>
    </div>
    `,
  data() {
    return {
      searchText: "",
    };
  },
  methods: {
    search() {
      // 在子组件中创建渲染完成的数据 传输出去用$emit
      // $emit 需要自定义一个事件.然后将要传的值当做事件对象放在第二个参数中
      // 然后在组件的使用中调用添加对应的事件v-on:transtext
      this.$emit("transtext", this.searchText);
    },
  },
});
