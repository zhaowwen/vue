/*
 * @Author: your name
 * @Date: 2021-08-05 11:21:17
 * @LastEditTime: 2021-08-05 14:13:49
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vuestudy\vue\15-组件\components\leftmenu.js
 */
Vue.component("left-menu", {
  props:{
      list:{
          type:Array,
          required:true,
      },
      position:{
          type:String,
          default:'left'
      }
  },
  template: `
  <div class="menuList" :style="showPosition">
    <div class="menu" v-for="(item,i) in list">
        <div class="title" @click="item.show=!item.show">
        {{item.name}}
        <i class="iconfont icon-topbrow"></i>
        </div>
        <ul v-show="item.show">
        <li v-for="(item1,j) in item.children" @click="activeParent=i,activeChildren=j"
            :class="{active:activeParent===i&&activeChildren===j}">
            {{item1.name}}
        </li>
        </ul>
    </div>
  </div>
    `,
  computed: {
      showPosition(){
          if(this.position==='left'){
                return "left:0"
          }else if(this.position==='right'){
              return "right:0"
          }else{
              return "left:"+this.position+"px"
          }
      }
  },
  data() {
    return {
      // 高亮索引
      activeTitleIndex: 0,
      activeChildren: 0,
      activeParent: 0,
    };
  },
});
