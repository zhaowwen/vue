/*
 * @Author: 全局混入 写一些普遍性的数据方法和处理 然后用script引入
 * @Date: 2021-08-06 16:51:01
 * @LastEditTime: 2021-08-06 16:52:47
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vuestudy\vue\20-混入\mixin\index.js
 */
// 混入
// 局部混入:在vue实例对象中定义属性 mixins:[{}]没有意义局部混入
// 全局混入：除了el其余都可以写
Vue.mixin({
  // 混入数据
  data() {
    // 一般只用到data对象的混入
    return {};
  },
  // 混入方法
  methods: {},
  // 全局混入后可以通过this.调用
});
