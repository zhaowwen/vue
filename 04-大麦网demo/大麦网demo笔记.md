## 大麦网的demo：
index：借助vue完成了大麦网演唱会详情的部分模仿设计
https://search.damai.cn/search.htm?spm=a2oeg.home.category.ditem_0.591b23e1KkjT8C&ctl=%E6%BC%94%E5%94%B1%E4%BC%9A&order=1&cty=%E5%8C%97%E4%BA%AC

#### 重点：v-for指令的使用；common.css设计； class属性绑定的两种方式：

### 1 v-for 指令的循环中 (item, index) in data
### 2 common.css 中提前设计好一些初始css 常用或者删除原始的自带样式
### 3 v-bind:class/:class 绑定属性的两种方式分别是：
        绑定对象的方式：
                {样式的class属性：满足条件后绑定方式（js表达式）}
        字符串的方式:--- 三目运算符
                "判断满足的条件（js表达式）？样式名：""

## 大麦网的demo 
### index01：设置datalist数据 异步的通过 axios获取 
### index02：异步 asxios获取，在生命周期数据创建阶段获取
     async created() {
            // 请求数据
            let {data:{pageData:{resultData}}} = await axios.get('./data/data.json')
            // .then(res=>{
            this.dataList = resultData
            console.log(resultData)
            // })
        },
     created(){
          
              axios.get('./data/data03.json')
                    .then(res => {
                       this.dataList = res.data["pageData"]["resultData"]
                      }
                    )
     }}
## index 03  给li注册@click事件 ：并将数据获取提取成单独的函数，并使用了过滤器进行数据的实时筛选 
    
    过滤器方法是.数据.filter(function(){
        // 这里进行数据的筛选并返回    
    })
### 匿名函数 和 箭头函数 中的this指代不同 匿名函数中的this指代的是window 箭头指代是就近对象
    匿名函数 function(){
    }
    箭头函数 ()=>{}
    function(x){
        return x*x
    }
    箭头函数 x => x*x

### index03中绑定的@click事件中 定义了数据获取和高亮显示的函数 这样就相当于 点击一次事件不管是否
### 切换都是进行数据的获取页面重新渲染 浪费资源


## index 04 让@click只进行点击事件 ，而不发生数据的更新和页面的渲染
##### 给Vue对象增阿了监听属性：
        监听属性虽是属性但其实是个方法 但是在页面中当做属性来用
            watch：{function(){
                监听是监听数据集变化 因此 这里的函数名必须 同data中数据变量一直
                cityactive(){
                }  //这个监听方法中可以传入两个值 newval oldval 
                    因此可以在这里写只要监听到数据的变化就进行数据的获取和页面的渲染
                    只有 监听到函数名对应的数据变化，才执行里面的内容
            }
            或者
            watch:(){
            
            }}
##### 给对象vue增加计算属性
        demo中在页面上添加了<li><h3>共{{count}}条信息</h3></li> 让其显示当前页面上的记录数据
        这个数据实时更新
        方法： 直接顶一个一个count 变量在 data中 但是这种方法定义的变量一般都具有普遍性，即页面中经常一直用到的
              而就像count 这样的只是在这里用一下 没有必要定义一个变量 并且这个count是由datalista中的数据长度
                决定的 就是说这个值是可以通过其余数据进行计算出来的 就像这种情况 vue提供了一个新的属性叫做computed
        computed 计算属性
        这种属性和监听属性一样：
        computed：{
            function（）{
                return
                }
            // 这里面的函数名同监听属性一样也是由数据决定 比如这里就是count也只能是count；
                这里函数的返回值就是函数名所对应变量的值
            count(){
            }
        }