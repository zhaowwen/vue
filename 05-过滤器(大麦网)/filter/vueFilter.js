// 全局过滤器 没有s 否则报错404————全局过滤器 放在单独文件夹
/*
* 一个页面中可以用vue去渲染不同的容器 全局过滤器对所有的vue对象适用而局部只对包含filter属性的vue适用
* */
Vue.filter('toFixed',function (val){
    return val.toFixed(2)
})
/*
*   转性别 01 男女
* */
Vue.filter("toGender",function (val){
    return val+""==="0"?"男":"女"
})
// 定义过滤器的好处 不用重复写数据处理的代码 只需要获取数据 如果对数据处理  就调用过滤器
// 引用过滤器：需要在 vuejs下面引入 filter.js